'use strict'

const _ = require('lodash')
const toString = (str) => require('util').inspect(str, {
  breakLength: Infinity
})

const parseParamValue = function (str) {
  try {
    str = JSON.parse((str || '').trim().replace(/'/g, '"'))
    return _.isObject(str) ? str : [str]
  } catch (e) {
    if (!/[:,]/.test(str) || /[[{]/.test(str)) {
      return [str]
    }

    return _.chain(str).split(',').compact().thru((ary) => {
      if (!/:/.test(str)) {
        return _.chain(ary).map(_.trim).value()
      }

      return _.chain(ary).reduce((ary, text) => {
        const p = text.indexOf(':')
        p < 0
          ? ary.push(text.trim())
          : ary[text.substring(0, p).trim()] = text.substring(p + 1).trim()
        return ary
      }, []).value()
    }).value()
  }
}

const setCond = (precond, ctx, next) => {
  if (!ctx.cond) ctx['cond'] = {}
  const error = []
  _.chain(precond).reduce((cond, rule) => {
    const key = rule.key
    if (cond[key]) return cond

    let val = ctx.query[key] || rule.default
    if (val == null) {
      if (rule.must) {
        error.push(`\`${key}\` must has value`)
      }
      return cond
    }

    const typeFn = rule.type || _.identity

    switch (rule.type.name) {
      case 'Array':
      case 'Object':
        val = parseParamValue(val)
        break
      case 'Number':
        if (!_.isNumber(val)) {
          val = val.trim().length ? +val : NaN
        }
        break
      case 'Date':
        val = new Date(val)
        break
      case 'Boolean':
        val = ['true', 'yes', 'y', '1', 'on'].indexOf(('' + val).trim().toLowerCase()) !== -1
        break
      default:
        val = typeFn(val)
    }

    const fixFn = rule.fix || _.identity
    const checkFn = rule.check || _.constant(true)
    cond[key] = _.chain(val).thru((v) => _.isArray(v) ? v : [v]).map((v) => {
      v = fixFn(v)
      if (!checkFn(v)) {
        let src = ctx.query[key]
        let hide = _.isArray(val) ? `${v} at ${src}` : src
        if (rule.should) {
          error.push(`\`${key}\` must has valid value, should \`${rule.should}\`, not \`${hide}\``)
        } else {
          error.push(`\`${key}\` must has valid value, not \`${hide}\``)
        }
      }
      return v
    }).thru(ary => rule.type.name === 'Array' ? _.sortedUniq(ary) : ary[0]).value()

    return cond
  }, ctx.cond).value()

  if (error.length) {
    ctx.status = 400
    ctx.body = toString(error)
    return
  }

  return next()
}

const setParam = _.curry((cond, key, value, ctx, next) => {
  const rule = _.find(cond, {key: key})
  if (!rule) return next()

  let val = value || rule.default
  const typeFn = rule.type || _.identity
  switch (typeFn.name) {
    case 'Date':
      val = new Date(val)
      break
    case 'Number':
      if (!_.isNumber(val)) {
        val = val.trim().length ? +val : NaN
      }
      break
    default:
      val = typeFn(val)
  }

  const fixFn = rule.fix || _.identity
  const checkFn = rule.check || _.constant(true)
  val = fixFn(val)

  if (!checkFn(val)) {
    let error = rule.should
      ? `\`${key}\` must has valid value, should \`${rule.should}\`, not \`${value}\``
      : `\`${key}\` must has valid value, not \`${value}\``

    ctx.status = 400
    ctx.body = error
    return
  }

  if (!ctx.cond) ctx.cond = {}
  ctx.cond[key] = val

  return next()
})

// help
const setHelp = _.curry((doc, ctx, next) => {
  ctx.body = _.chain(doc).map(o => _.pick(o, ['key', 'type.name', 'must', 'should', 'desc'])).value()
})

module.exports = {
  setCond: _.curry(setCond),
  setParam: _.curry(setParam),
  setHelp: _.curry(setHelp)
}
